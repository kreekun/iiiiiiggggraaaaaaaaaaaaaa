﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testUI : MonoBehaviour {

	public Button bt1;
	public Button bt2;
	public Toggle tg;
	public Image img;

	// Use this for initialization
	void Start () {
		tg.isOn = PlayerPrefs.GetInt ("on") == 1;
	}

	public void OnToggleChange (bool status) {

		bt1.gameObject.SetActive (status);
		bt2.gameObject.SetActive (status);

		PlayerPrefs.SetInt ("on", status ? 1 : 0);

	}

	public void RedColorCanal (float data) {
		img.color = new Color (data, img.color.g, img.color.b);
	}

	public void GreenColorCanal (float data) {
		img.color = new Color (img.color.r, data, img.color.b);
	}

	public void BlueColorCanal (float data) {
		img.color = new Color (img.color.r, img.color.g, data);
	}



}

	

