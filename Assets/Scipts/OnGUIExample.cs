﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnGUIExample : MonoBehaviour {

    private Color LabelTextColor = Color.white;

	void OnGUI()
	{
		// Make a background box
		GUI.Box(new Rect(10, 10, 100, 120), "Some box");

		if (GUI.Button(new Rect(20, 40, 80, 20), "Red label"))
		{
            LabelTextColor = Color.red;
		}

		// Make the second button.
		if (GUI.Button(new Rect(20, 70, 80, 20), "Green label"))
		{
            LabelTextColor = Color.green;
		}

        GUI.contentColor = LabelTextColor;
        GUI.Label(new Rect(200, 200, 80, 20),"Im label");
	}
}
