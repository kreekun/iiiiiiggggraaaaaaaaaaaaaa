using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConrtoller : MonoBehaviour {
	public Transform[] SpawnArray;

	public GameObject EnemyPrefab;

	//эта переменная используется для созданния ссылки на Player
	public Transform PlayerForPrefab;

	public float TimeEnemySpawn;

	float time;



	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {

		time += Time.deltaTime;

		if (time >= TimeEnemySpawn) {
			GameObject Enemy = Instantiate (EnemyPrefab, SpawnArray [Random.Range (0, SpawnArray.Length)].position, Quaternion.identity);
			// делаем ссылку на нашего игрока и "передаём" её (через присваивание) в скрипт движения Emeny
			Enemy.GetComponent<EnemyController> ().Player = PlayerForPrefab;
			time = 0;
		}
				
	}
}
