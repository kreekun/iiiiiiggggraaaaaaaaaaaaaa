﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
	private Rigidbody bulletRB; //переменная для созданя ссылки на компонент <Rigidbody> объекта Bullet (префаб)
	public float SpeedBullet;  // переменная для храения значения скорости движения пули. (задаём в Инспекторе)


	// Use this for initialization
	void Start () {
		bulletRB = GetComponent<Rigidbody> (); //присваиваем переменной ссылку на компонент объекта
	}
	
	//используем метод FixedUpdate, потому в нём обрабытываются события физики. (за физику отвечает компонент Rigidboby)
		void FixedUpdate () {

		//метод MovePosition 
		bulletRB.MovePosition (transform.position + transform.forward * SpeedBullet * Time.fixedDeltaTime);

	}
}
