﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public Transform Player;
	public Rigidbody rb;
	public int speed;


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();

		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rb.MovePosition (transform.position + (Player.position - transform.position).normalized * speed * Time.fixedDeltaTime);
		rb.rotation = Quaternion.LookRotation ((Player.position - transform.position).normalized);
	}
}
