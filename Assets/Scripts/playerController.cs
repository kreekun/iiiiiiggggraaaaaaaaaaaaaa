﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public Rigidbody rb;            // вводим переменную типа "Rigidbody", чтобы присвоить ей ссылку на компонент Rigidbody нашего Player-(a). 




	//подумай куда это вставить!!! public сделает поле переменной rb доступным в инспекторе и тогда мы сможем с помощью компьютерной мыши перетянуть объект Player 
	//подумай куда это вставить!!!из окна Hierarchy поле rb инспектора. При этом в окне Hierarchy должен быть выдеделе 





	public int speed;               //переменная для задания скорости перемещения игрока
	public GameObject BulletPrefab;
	public Transform ShooterPoint;



	// Use this for initialization
	void Start () {
		
		rb = GetComponent<Rigidbody>(); // с помощью GetComponent получаем 
		
	}

	private void InstantiateBullet () {
		GameObject Bullet = Instantiate (BulletPrefab, ShooterPoint.position, transform.rotation);
	}

		
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if (Input.GetKey (KeyCode.W)) {
			
			Debug.Log (" keys W down");

			rb.MovePosition (transform.position + Vector3.forward * speed * Time.fixedDeltaTime);
		}

		if (Input.GetKey (KeyCode.S)) {

			Debug.Log (" keys S down");

			rb.MovePosition (transform.position + (-Vector3.forward) * speed * Time.fixedDeltaTime);
		}

		if (Input.GetKey (KeyCode.A)) {

			Debug.Log (" keys A down");

			rb.MovePosition (transform.position + Vector3.left * speed * Time.fixedDeltaTime);
		}

		if (Input.GetKey (KeyCode.D)) {

			Debug.Log (" keys D down");

			rb.MovePosition (transform.position + (-Vector3.left) * speed * Time.fixedDeltaTime);
		}
	 
		if (Input.GetKey (KeyCode.UpArrow)) {
			Debug.Log (" keys UP down");
			rb.rotation = Quaternion.Euler (0, 0, 0);
			InstantiateBullet ();


		}
	
		if (Input.GetKey (KeyCode.RightArrow)) {
			Debug.Log (" keys UP down");
			rb.rotation = Quaternion.Euler (0, 90, 0);
			InstantiateBullet ();

		}
	
		if (Input.GetKey (KeyCode.DownArrow)) {
			Debug.Log (" keys UP down");
			rb.rotation = Quaternion.Euler (0, 180, 0);
			InstantiateBullet ();

		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			Debug.Log (" keys UP down");
			rb.rotation = Quaternion.Euler (0, 270, 0);
			InstantiateBullet ();

		}
	}

}
