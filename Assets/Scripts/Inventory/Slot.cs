﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
	public bool Empty;
	private Transform ParentSlot;


	#region IDropHandler implementation
	public void OnDrop (PointerEventData eventData)
	{
		if (Empty) {
			eventData.pointerDrag.GetComponent<ItemCell> ().SetSlot (this);	
		}
		
	}
	#endregion
}
