﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemCell : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
	public Slot slot;
	public Item item;
	private Transform OriginalParent;

	public void SetItem (Item item) {
		this.item = item;
		GetComponent<Image> ().sprite = item.spr;

	}

	public void SetSlot(Slot slot) {
		transform.SetParent (slot.transform);
		//transform.localPosition = 
	}


	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		OriginalParent = slot.transform;
		transform.SetParent (slot.transform.parent.parent);
		GetComponent<CanvasGroup> ().blocksRaycasts = false; //отключаем свойство blocksRaycasts, чтобы наш элемент (именно тот который мы тянем) не перекрывал "лучи" подающие на наши слоты и которые выдают информацию о том слоте на котором брошен наш элемент, которые тянем мышкой
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		transform.position = (Vector3)eventData.position;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		//transform.SetParent (OriginalParent);
		transform.localPosition = Vector3.zero;
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
	}

	#endregion
}
