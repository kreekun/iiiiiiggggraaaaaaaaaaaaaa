﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable] //ключевое слово для созданных нами "типов", чтобы Unity мог отоброжать их в окне Inspector и мы могли редактировать значения прямо в этом окне 
public class Item
{
	public string name;
	public Sprite spr;
	public ItemType type;
}

public enum ItemType
{
	Bullet, Grenade
}
