﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour 
{
	public int SlotCount;
	public Item[] items;
	private Slot[] slots;
	public Transform InventoryPanel;
	public GameObject slot;
	public GameObject itemcell;


	void Start ()
	{
		slots = new Slot[SlotCount]; 
		for (int i = 0; i < SlotCount; i++)
			slots [i] = Instantiate (slot, InventoryPanel).GetComponent<Slot>();
		AddItemOnInventoryCell (ItemType.Grenade);
		AddItemOnInventoryCell (ItemType.Bullet);
	}

	public void AddItemOnInventoryCell (ItemType type)  
	{
		Item temp = null;
		for (int i = 0; i < items.Length; i++) {
			if (items [i].type == type) {
				temp = items [i]; 
				break;
			}
		}
		for (int i = 0; i < slots.Length; i++) {
			if (slots [i].Empty) {
				ItemCell cell = Instantiate (itemcell).GetComponent<ItemCell> ();
				cell.transform.SetParent(slots[i].transform);
				cell.transform.localPosition = Vector3.zero;
				cell.SetItem (temp);
				cell.slot = slots [i];
				slots [i].Empty = false;
				break;
			}
		}
	} 
}
